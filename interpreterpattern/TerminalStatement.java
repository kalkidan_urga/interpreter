/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interpreter2;

/**
 *
 * @author henok
 */
class TerminalStatement implements Statement  
{ 
    String data; 
  
    public TerminalStatement(String data) 
    { 
        this.data = data;  
    } 
  
    public boolean interpreter(String con)  
    { 
        if(con.contains(data)) 
        { 
            return true; 
        } 
        else
        { 
            return false;   
        } 
    } 
} 
