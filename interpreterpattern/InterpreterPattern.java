package interpreter2;
/**
 *
 * @author KALKIDAN
 */
class InterpreterPattern 
{ 
  
    public static void main(String[] args)  
    { 
        Statement person1 = new TerminalStatement("henok"); 
        Statement person2 = new TerminalStatement("Kal"); 
        Statement person3 = new TerminalStatement("kibr"); 
        Statement person4 = new TerminalStatement("Grmay"); 
        
        Statement isSingle = new OrStatement(person1, person2);              
        Statement isMarried = new MultipleStatement(person2, person3);     
  
        System.out.println(isSingle.interpreter("henok")); 
        System.out.println(isSingle.interpreter("Kal")); 
        System.out.println(isSingle.interpreter("perso4")); 
          
        System.out.println(isMarried.interpreter("Committed, Vikram")); 
        System.out.println(isMarried.interpreter("Single, kibr")); 
  
    } 
} 